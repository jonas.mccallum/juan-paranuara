#!/bin/bash

# This follows original instructions from https://docs.docker.com/install/linux/docker-ce/ubuntu/
# and https://docs.docker.com/compose/install/#install-compose

# This will not work on my computer as i use Linux mint... but it should work on yours
# As per OFFICIAL docker docs
#Note: The lsb_release -cs sub-command below returns the name of your Ubuntu distribution, such as xenial.
#Sometimes, in a distribution like Linux Mint, you might need to change $(lsb_release -cs) to your parent Ubuntu
#distribution. For example, if you are using Linux Mint Rafaela, you could use trusty.

ubuntu_version=`lsb_release -cs`

# If you happen to use linux mint like me, then uncomment this line
#ubuntu_version=xenial

sudo apt-get remove docker docker-engine docker.io
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $ubuntu_version \
   stable"

sudo apt-get update
sudo apt-get install docker-ce

sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

docker-compose up