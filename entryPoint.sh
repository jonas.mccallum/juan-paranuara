#!/bin/bash

if [ "$RELOAD_DATA" == "1" ]; then
    echo "Will reload data... it will take a bit"
else
    echo "No data reload will happen"
fi

cd /opt/moriano/src
python server.py