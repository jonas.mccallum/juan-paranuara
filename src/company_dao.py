__author__ = 'moriano'
from base_dao import BaseDAO


class CompanyDAO(BaseDAO):

    def __init__(self):
        super().__init__()

    def clear(self):
        """
        Removes every entry in the table
        :return:
        """
        sql = "DELETE FROM companies"
        self._cursor.execute(sql)

    def get_by_id(self, company_id) -> dict:
        """
        Gets a company by id, returns a dict with the result,
        the dict might be empty if no match is found
        :param company_id:
        :return:
        """
        output = {}
        sql = "SELECT id, name FROM companies WHERE id = ?"
        rs = self._cursor.execute(sql, company_id)
        for entry in rs:
            output = {"id": entry[0], "name": entry[1]}
        return output

    def load_data(self, companies) -> int:
        """
        Given a list of dictionaries obtained from the json file,
        load the data in the company database, notice that this
        assumes that the companies table will be empty, you might
        want to call to the clear() method before.

        Returns the total number of companies inserted
        :param companies:
        :return:
        """
        inserted = 0
        for company in companies:
            sql = "INSERT INTO companies (id, name) VALUES (?, ?)"
            self._cursor.execute(sql, (company["index"], company["company"]))
            inserted += 1
        return inserted

    def get_all_employees(self, company_id):
        """
        Given a company id, it provides back all the employees.

        It is worth noting that there is no definition of what an "Employee"
        constitutes here (the full person object as in the person.json file?
        just the name?).

        As it is a fuzzy spec, I will simply return everything that is
        in the people table
        :param company_id:
        :return:
        """

        sql = """SELECT p.* FROM people p
                    INNER JOIN companies c ON c.id = p.company_id
                    WHERE c.id = ?"""

        employees = []
        rs = self._cursor.execute(sql, (company_id,))
        # From https://stackoverflow.com/questions/16519385/output-pyodbc-cursor-results-as-python-dictionary
        columns = [column[0] for column in self._cursor.description]
        for entry in rs:
            employees.append(dict(zip(columns, entry)))

        return employees