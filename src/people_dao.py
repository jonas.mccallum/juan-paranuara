__author__ = 'moriano'
from base_dao import BaseDAO
from food_dao import FoodDAO

class PeopleDAO(BaseDAO):

    def __init__(self, food_dao: FoodDAO):
        super().__init__()
        self.__food_dao = food_dao

    def clear(self):
        """
        Removes every entry in the people table
        :return:
        """
        sql = "DELETE FROM people"
        self._cursor.execute(sql)


    def get_common_friends_with_filters(self, person_id1, person_id2, eye_color="brown", alive=1):
        """
        Given 2 people, provide their information (Name, Age, Address, phone)
        and the list of their friends in common which have brown eyes and are still alive.
        :param person_id1:
        :param person_id2:
        :param eye_color:
        :param alive:
        :return:
        """

        sql = """
        SELECT p.* FROM people p
        WHERE p.id  IN (select friend_id from people_people WHERE people_id = ?)
            AND p.alive = ? AND p.eye_color = ?

        INTERSECT

        SELECT p.* FROM people p
        WHERE p.id  IN (select friend_id from people_people WHERE people_id = ?)
              AND p.alive = ? AND p.eye_color = ?;
        """



        person_1 = self.get_by_id(person_id1)
        person_2 = self.get_by_id(person_id2)

        results = {"person1": person_1,
                   "person2": person_2,
                   "friends": []}

        rs = self._cursor.execute(sql, (person_id1, alive, eye_color, person_id2, alive, eye_color))
        columns = [column[0] for column in self._cursor.description]
        for entry in rs:
            results["friends"].append((dict(zip(columns, entry))))

        return results

    def get_by_id(self, person_id) -> dict:
        """
        Gets a person by its id, returning a dict, if nothing is available,
        then an empty dict is returned
        :param person_id:
        :return:
        """
        sql = "SELECT id, name, company_id, age, eye_color, alive, address FROM people WHERE id = ?"
        rs = self._cursor.execute(sql, (person_id,))
        result = {}
        columns = [column[0] for column in self._cursor.description]
        for entry in rs:
            result = (dict(zip(columns, entry)))
        return result

    def get_preferred_food(self, person_id) -> dict:
        """
        Given 1 people, provide a list of fruits and vegetables they like.
        :param person_id:
        :return:
        """
        sql = """SELECT p.*, f.kind as food_kind , f.name as food_name
                FROM people p
                INNER JOIN food_people fp ON fp.people_id = p.id
                INNER JOIN foods f ON f.name = fp.food_name
                where p.id = ?"""

        rs = self._cursor.execute(sql, (person_id, ))

        columns = [column[0] for column in self._cursor.description]
        results = []
        for entry in rs:
            results.append(dict(zip(columns, entry)))

        return results

    def clear_friends(self):
        """
        Removes all the entries in the people_people table
        :return:
        """
        sql = "DELETE FROM people_people"
        self._cursor.execute(sql)

    def link_friends(self, person_id, friend_id):
        """
        Links two people as friends, where friend_id becomes a friend of person_id.

        Notice! the fact that A is friend of B does NOT imply that B is friend of A,
        if such relation needs to be represented, then two entries will be needed
        :param person_id:
        :param friend_id:
        :return:
        """
        if person_id == friend_id:
            print("You cannot be friend with yourself, ignoring")
        else:
            sql = "INSERT INTO people_people (people_id, friend_id) VALUES (?, ?)"

            self._cursor.execute(sql, (person_id, friend_id))

    def load_data(self, people) -> int:
        """
        Given a list of people, write the information into the people table,
        notice that this assumes that the table is empty, you might want to
        call the clear method before.

        Notice that the raw definition of aperson is

        {
           '_id':'595eeb9b96d80a5bc7afb106',
           'index':0,
           'guid':'5e71dc5d-61c0-4f3b-8b92-d77310c7fa43',
           'has_died':True,
           'balance':'$2,418.59',
           'picture':'http://placehold.it/32x32',
           'age':61,
           'eyeColor':'blue',
           'name':'Carmella Lambert',
           'gender':'female',
           'company_id':58,
           'email':'carmellalambert@earthmark.com',
           'phone':'+1 (910) 567-3630',
           'address':'628 Sumner Place, Sperryville, American Samoa, 9819',
           'about':'Non duis dolore ad enim. Est id reprehenderit cupidatat tempor excepteur. Cupidatat labore incididunt nostrud exercitation ullamco reprehenderit dolor eiusmod sit exercitation est. Voluptate consectetur est fugiat magna do laborum sit officia aliqua magna sunt. Culpa labore dolore reprehenderit sunt qui tempor minim sint tempor in ex. Ipsum aliquip ex cillum voluptate culpa qui ullamco exercitation tempor do do non ea sit. Occaecat laboris id occaecat incididunt non cupidatat sit et aliquip.\r\n',
           'registered':'2016-07-13T12:29:07 -10:00',
           'tags':[
              'id',
              'quis',
              'ullamco',
              'consequat',
              'laborum',
              'sint',
              'velit'
           ],
           'friends':[
              {
                 'index':0
              },
              {
                 'index':1
              },
              {
                 'index':2
              }
           ],
           'greeting':'Hello, Carmella Lambert! You have 6 unread messages.',
           'favouriteFood':[
              'orange',
              'apple',
              'banana',
              'strawberry'
           ]
        }

        :param people:
        :return:
        """

        inserted = 0

        """
        All the different types of food are
            {'beetroot', 'celery', 'apple', 'cucumber', 'orange', 'banana', 'carrot', 'strawberry'}

        Of which, fruits are
            apple, orange, banana, strawberry
        Vegetables are:
            beetroot, celery, cucumber, carrot
        """

        fruits = {"apple", "orange", "banana", "strawberry"}
        vegetables = {"beetroot", "celery", "cucumber", "carrot", "lettuce"}

        # Note! I have added lettuce to vegetables because the api specifies it in the example...
        # even though i cannot find any user who likes lettuce
        # Personal note, this solution will not work with tomatoes as some people consider them
        # fruits AND vegetables at the same time :)

        current_food = self.__food_dao.get_all()
        current_food_names = []
        for current in current_food:
            current_food_names.append(current["name"])

        for person in people:
            person_id = person["index"]

            for food in person["favouriteFood"]:
                kind = None
                if food in fruits:
                    kind = "fruit"
                elif food in vegetables:
                    kind = "vegetable"
                else:
                    print("Watch out. what is a ", food)

                # New type of food! we need to store it
                if kind and food not in current_food_names:
                    print("A new type of food found is inserted!", food, "kind", kind)
                    current_food_names.append(food)
                    self.__food_dao.insert(food, kind)

                # And in any case, we need to keep track of the relation
                self.__food_dao.insert_relation(food, person_id)

            for friend in person["friends"]:
                friend_id = friend["index"]
                self.link_friends(person_id, friend_id)

            sql = """INSERT INTO people
                    (id, name, company_id, age, eye_color, alive, address, phone)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)"""

            alive = 0 if person["has_died"] else 1
            self._cursor.execute(sql, (person_id,
                                       person["name"],
                                       person["company_id"],
                                       person["age"],
                                       person["eyeColor"],
                                       alive,
                                       person["address"],
                                       person["phone"]))

            inserted += 1
            if inserted % 25 == 0:
                print("Loaded", inserted, " people so far")

        return inserted