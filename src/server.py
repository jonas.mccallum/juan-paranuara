__author__ = 'moriano'
from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify

from company_dao import CompanyDAO
from people_dao import PeopleDAO
from food_dao import FoodDAO
import re
from collections import OrderedDict
import os
from importer import import_companies
from importer import import_users

app = Flask(__name__)
app.debug = True

company_dao = CompanyDAO()
food_dao = FoodDAO()
people_dao = PeopleDAO(food_dao)


@app.route("/")
def app_help():
    """
    Simple helper method to get a list of all available urls calls...
    :return:
    """
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static':
            help = app.view_functions[rule.endpoint].__doc__
            if help:
                help = re.sub(".*return.*\n", "", help).replace("\n", '<br/>')
                func_list[rule.rule] = help

    instructions = OrderedDict(func_list)

    urls = sorted(instructions.keys())
    return render_template("api.html", urls=urls, help=instructions)

@app.route("/api/people/friends/<person_id1>/<person_id2>")
def common_friends(person_id1, person_id2):
    """
    Given two people, give a list of common friends with filters.

    <pre>
    Filters can be specified as
        ?eye_color=brown  <-- any string will do
        &alive=1   <-- 1 means alive, 0 means dead
    </pre>

    if no query parameters are provided, then the default used is eye_color = "brown", alive = 1

    Try me <a href="/api/people/friends/20/7">/api/people/friends/20/7</a>
    Try me <a href="/api/people/friends/20/7?eye_color=blue&alive=0">/api/people/friends/20/7?eye_color=blue&alive=0</a>

    :return:
    """
    eye_color = request.args.get("eye_color", "brown")
    alive = request.args.get("alive", 1)
    employees = people_dao.get_common_friends_with_filters(person_id1, person_id2, eye_color, alive)
    return jsonify(employees)


@app.route("/api/company/<company_id>/employees")
def employees_by_company(company_id: int):
    """
    Given a company, return a list with all the employees

    Try me <a href="/api/company/5/employees">/api/company/5/employees</a>
    :return:
    """
    employees = company_dao.get_all_employees(int(company_id))
    return jsonify(employees)


@app.route("/api/person/<person_id>")
def person_by_id(person_id: int):
    """
    A simple method to grab a person by its id

    Try me <a href="/api/person/42">/api/person/42</a>
    :param person_id:
    :return:
    """
    return jsonify(people_dao.get_by_id(int(person_id)))

@app.route("/api/person/food/<person_id>")
def person_food_preferences(person_id: int):
    """
    Given a person, we will a list of fruits and vegetables they like
    This endpoint must respect this interface for the output:
    <pre>
        {
        "username": "Ahi",
        "age": "30",
        "fruits": ["banana", "apple"],
        "vegetables": ["beetroot", "lettuce"]
        }
    </pre>

    Try me <a href="/api/person/food/42">/api/person/food/42</a>
    :param person_id:
    :return:
    """
    raws = people_dao.get_preferred_food(person_id)
    user_to_result = {}  # Key is user id, value is the cleaned result
    user_id = -1
    for raw in raws:
        user_id = raw["id"]
        cleaned = user_to_result.get(user_id, {"username": raw["name"],
                                               "age": raw["age"],
                                               "vegetables": [],
                                               "fruits": []})

        food_kind = raw["food_kind"]
        food_name = raw["food_name"]
        if food_kind == "fruit":
            cleaned["fruits"].append(food_name)
        elif food_kind == "vegetable":
            cleaned["vegetables"].append(food_name)

        user_to_result[user_id] = cleaned
    return jsonify(user_to_result.get(user_id, {}))
if __name__ == '__main__':
    reload = os.environ.get("RELOAD_DATA", 0)
    if reload == "1":
        print("Reloading data... will take a bit")
        import_companies(CompanyDAO())
        import_users()

    app.run(debug=False, host='0.0.0.0')
