__author__ = 'moriano'

import json
from company_dao import CompanyDAO
from people_dao import PeopleDAO
from food_dao import FoodDAO


def import_companies(company_dao: CompanyDAO):
    with open('../resources/companies.json') as json_data:
        companies = json.load(json_data)
        company_dao.clear()
        imported = company_dao.load_data(companies)
        print("Imported", imported, "companies")


def import_users():
    food_dao = FoodDAO()
    people_dao = PeopleDAO(food_dao)

    with open("../resources/people.json") as people_data:
        food_dao.clear()
        people_dao.clear_friends()
        people_dao.clear()
        people = json.load(people_data)
        imported = people_dao.load_data(people)
        print("Imported", imported, "people")




