__author__ = 'moriano'
import sqlite3


class BaseDAO():
    """
    A base class that handles the basic of database connections
    """
    def __init__(self):
        self._conn = sqlite3.connect('../db/database.db', check_same_thread=False)
        self._conn.isolation_level = None  # Sets autocommit to true
        self._cursor = self._conn.cursor()