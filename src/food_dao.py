__author__ = 'moriano'

from base_dao import BaseDAO

class FoodDAO(BaseDAO):
    """
    A simple dao in charge of basic operations for food and food_people table
    """

    def __init__(self):
        super().__init__()

    def insert(self, name, kind):
        sql = "INSERT INTO foods (name, kind) VALUES (?, ?)"
        self._cursor.execute(sql, (name, kind))

    def insert_relation(self, food_name, people_id):
        sql = "INSERT INTO food_people (food_name, people_id) VALUES (?, ?)"
        self._cursor.execute(sql, (food_name, people_id))

    def get_all(self) -> []:
        """
        Returns a list with all the available foods
        :return:
        """
        sql = "SELECT name, kind FROM foods"
        rs = self._cursor.execute(sql)
        results = []
        for entry in rs:
            results.append({"name": entry["name"], "kind": entry["kind"]})

        return results

    def clear(self):
        sql = "DELETE FROM foods"
        self._cursor.execute(sql)

        sql = "DELETE FROM food_people"
        self._cursor.execute(sql)