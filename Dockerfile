FROM python:3.6

RUN apt-get update
RUN apt-get install sqlite3

RUN pip install flask
RUN mkdir -p /opt/moriano/db & mkdir -p /opt/moriano/src & mkdir -p /opt/moriano/resources
COPY db/database.db /opt/moriano/db/database.db
ADD src/ /opt/moriano/src
ADD resources/ /opt/moriano/resources
COPY entryPoint.sh /tmp
RUN chmod +x /tmp/entryPoint.sh

ENTRYPOINT "/tmp/entryPoint.sh"

